﻿<#	
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.121
	 Created on:   	8/4/2016 7:16 PM
	 Created by:   	Ben Claussen
	 Filename:     	PSASAAPI.psm1
	-------------------------------------------------------------------------
	 Module Name: PSASAAPI
	===========================================================================
#>


function Save-ASARunningConfig {
<#
	.SYNOPSIS
		A brief description of the Save-ASARunningConfig function.
	
	.DESCRIPTION
		A detailed description of the Save-ASARunningConfig function.
	
	.PARAMETER Force
		Skip confirmation of saving config
	
	.EXAMPLE
				PS C:\> Save-ASARunningConfig
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(ConfirmImpact = 'High',
				   SupportsShouldProcess = $true)]
	[OutputType([pscustomobject])]
	param
	(
		[switch]$Force
	)
	
	$uri = BuildNewURI -Segments 'commands', 'writemem'
	
	if ($Force -and $WhatIfPreference) {
		throw "Cannot use 'Force' and 'WhatIf' parameters simultaneously!"
	}
	
	if ($Force -or ($PSCmdlet.ShouldProcess((Get-ASAHostname), 'Save running config to startup config'))) {
		InvokeASARequest -URI $uri -Method POST -WhatIf:$WhatIfPreference
	}
}

function Invoke-ASACLICommand {
<#
	.SYNOPSIS
		Executes one or more CLI commands
	
	.DESCRIPTION
		A detailed description of the Invoke-ASACLICommand function.
	
	.PARAMETER Commands
		A description of the Commands parameter.
	
	.PARAMETER Force
		A description of the Force parameter.
	
	.EXAMPLE
		PS C:\> Invoke-ASACLICommand -Commands $value1
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(ConfirmImpact = 'High',
				   SupportsShouldProcess = $true)]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string[]]$Commands,
		
		[switch]$Force
	)
	
	# TODO: This function returns a broken connection
	Write-Warning "This is broken"
	
	if ($Force -or $pscmdlet.ShouldProcess((Get-ASAHostname), "$(@($Commands).count) CLI operations")) {
		$URI = BuildNewURI -Segments 'cli'
		
		$body = @{
			'commands' = [System.Collections.ArrayList]::new($commands)
		}
		
		Write-Verbose ($body | ConvertTo-Json -Compress)
		
		InvokeASARequest -URI $URI -Body $body -Method POST
	}
}



function Get-ASAClock {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param ()
	
	$URI = BuildNewURI -Segments 'monitoring', 'clock'
	
	InvokeASARequest -URI $URI
}


#Export-ModuleMember "*-*"
Export-ModuleMember "*"
















