﻿

function Get-ASANATAuto {
<#
	.SYNOPSIS
		Fetch an Auto NAT rule by ID or All
	
	.DESCRIPTION
		A detailed description of the Get-ASANATAuto function.
	
	.PARAMETER objectId
		A description of the objectId parameter.
	
	.PARAMETER All
		A description of the All parameter.
	
	.PARAMETER Limit
		A description of the Limit parameter.
	
	.PARAMETER Offset
		A description of the Offset parameter.
	
	.EXAMPLE
		PS C:\> Get-ASANATAuto
	
	.OUTPUTS
		pscustomobject, pscustomobject, pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'Default')]
	[OutputType([pscustomobject], ParameterSetName = 'Default')]
	[OutputType([pscustomobject], ParameterSetName = 'Single')]
	[OutputType([pscustomobject], ParameterSetName = 'All')]
	param
	(
		[Parameter(ParameterSetName = 'Single',
				   Mandatory = $true)]
		[string]$ObjectId,
		
		[Parameter(ParameterSetName = 'All')]
		[switch]$All,
		
		[Parameter(ParameterSetName = 'Default')]
		[ValidateRange(1, 100)]
		[byte]$Limit,
		
		[Parameter(ParameterSetName = 'Default')]
		[uint16]$Offset
	)
	
	$URISegments = [System.Collections.ArrayList]::new(@('nat', 'auto'))
	
	switch ($PsCmdlet.ParameterSetName) {
		'Single' {
			$URI = BuildNewURI -Segments ($URISegments + $ObjectId)
			break
		}
		
		'All' {
			# This will run a loop
			break
		}
		
		'Default' {
			#TODO: Place script here
			break
		}
	}
	
	if ($PSCmdlet.ParameterSetName -ne 'All') {
		return InvokeASARequest -URI $URI
	}
	
	return LoopASARequestUntilAllItemsObtained -URISegments $URISegments
}










