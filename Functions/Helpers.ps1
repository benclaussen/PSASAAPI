﻿

function CheckASAIsConnected {
	[CmdletBinding()]
	param ()
	
	if ((-not $script:ASAConfig)) { #} -or (-not $script:ASAConfig.Connected)) {
		throw "Not connected to an ASAAPI! Please run 'Connect-ASAAPI'"
	}
}

function InvokeASARequest {
<#
	.SYNOPSIS
		Sends the HTTP request to the ASA
	
	.DESCRIPTION
		Generates the necessary parameters to send a properly formed API request to the ASA
	
	.PARAMETER URI
		UriBuilder object returned from BuildNewUri
	
	.PARAMETER Headers
		Any HTTP headers to be included in the request. Authorization will be added automatically.
	
	.PARAMETER Body
		Hashtable body data which will be converted to JSON for the request
	
	.PARAMETER Timeout
		Request timeout in seconds
	
	.PARAMETER Method
		HTTP Method
	
	.PARAMETER TokenRequest
		A description of the TokenRequest parameter.
	
	.EXAMPLE
		PS C:\> InvokeASARequest -URI $value1
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(SupportsShouldProcess = $true)]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.UriBuilder]$URI,
		
		[Hashtable]$Headers = @{ },
		
		[pscustomobject]$Body = $null,
		
		[ValidateRange(0, 60)]
		[uint16]$Timeout = 5,
		
		[ValidateSet('GET', 'PATCH', 'PUT', 'POST', 'DELETE', IgnoreCase = $true)]
		[string]$Method = 'GET',
		
		[switch]$TokenRequest
	)
	
	$creds = Get-ASACredentials
	
	$splat = @{
		'Method' = $Method
		'Uri' = $URI.Uri # This property auto generates the scheme, hostname, path, and query
		'Headers' = $Headers
		'TimeoutSec' = $Timeout
		'ContentType' = 'application/json'
		'ErrorAction' = 'Stop'
		'Verbose' = $VerbosePreference
	}
	
	if ($Body) {
		Write-Verbose $($Body | ConvertTo-Json -Compress)
		$null = $splat.Add('Body', ($Body | ConvertTo-Json -Compress))
	}
	
	if ($TokenRequest) {
		if (-not $Headers.Authorization) {
			# Convert the stored credentials to HTTP Basic authorization header if not already defined
			Write-Verbose 'Adding authorization to headers'
			$null = $Headers.Authorization = "Basic {0}" -f $(ConvertToBase64 -String $("{0}:{1}" -f $creds.Username, $creds.GetNetworkCredential().Password))
		}
		
		Invoke-WebRequest @splat
	} else {
		if (-not $Headers.'X-Auth-Token') {
			# Add the token if not already defined
			Write-Verbose 'Adding token to headers'
			$null = $Headers.'X-Auth-Token' = Get-ASAToken
		}
		
		Invoke-RestMethod @splat
	}
}

function BuildNewURI {
<#
	.SYNOPSIS
		Create a new URI for the ASA
	
	.DESCRIPTION
		A detailed description of the BuildNewURI function.
	
	.PARAMETER Hostname
		Hostname of the ASA API
	
	.PARAMETER Segments
		Array of strings for each segment in the URL path
	
	.PARAMETER Parameters
		Hashtable of query parameters to include
	
	.PARAMETER HTTPS
		Whether to use HTTPS or HTTP
	
	.EXAMPLE
				PS C:\> BuildNewURI
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([System.UriBuilder])]
	param
	(
		[Parameter(Mandatory = $false)]
		[string]$Hostname,
		
		[Parameter(Mandatory = $false)]
		[string[]]$Segments,
		
		[Parameter(Mandatory = $false)]
		[hashtable]$Parameters,
		
		[Parameter(Mandatory = $false)]
		[boolean]$HTTPS = $true
	)
	
	CheckASAIsConnected
	
	if (-not $Hostname) {
		$Hostname = Get-ASAHostname
	}	
	
	# Begin a URI builder with HTTP/HTTPS and the provided hostname
	$uriBuilder = [System.UriBuilder]::new($(if ($HTTPS) {
				'https'
			} else {
				'http'
			}), $Hostname)
	
	# Generate the path by trimming excess slashes and whitespace from the $segments[] and joining together
	$uriBuilder.Path = "api/{0}" -f ($Segments.ForEach({
				$_.trim('/').trim()
			}) -join '/')
	
	if ($parameters) {
		# Loop through the parameters and use the HttpUtility to create a Query string
		$URIParams = [System.Web.HttpUtility]::ParseQueryString([String]::Empty)
		
		foreach ($param in $Parameters.GetEnumerator()) {
			$URIParams[$param.Key] = $param.Value
		}
		
		$uriBuilder.Query = $URIParams.ToString()
	}
	
	# Return the entire UriBuilder object
	$uriBuilder
}

function BuildLimitOffsetParameters {
	[CmdletBinding()]
	[OutputType([hashtable])]
	param
	(
		[Parameter(Position = 0)]
		[uint16]$Limit,
		
		[Parameter(Position = 1)]
		[uint16]$Offset
	)
	
	$Parameters = @{
	}
	if ($Limit) {
		$Parameters.Add('limit', $limit)
	}
	
	if ($Offset) {
		$Parameters.Add('offset', $Offset)
	}
	
	$parameters
}

function LoopASARequestUntilAllItemsObtained {
	[CmdletBinding()]
	param
	(		
		[Parameter(Mandatory = $true)]
		[array]$URISegments
	)
	
	Write-Verbose "Obtaining first result set"
	$URI = BuildNewURI -Segments $URISegments
	$results = InvokeASARequest -URI $URI
	
	if ($results.rangeInfo.total -le 100) {
		Write-Verbose "Total results are less than or equal to 100"
		return $results
	} else {
		Write-Verbose "Total results are greater than 100 ($($results.rangeInfo.total)). Looping to obtain remaining results"
		
		# Initial collection
		$items = [System.Collections.ArrayList]::new($results.items)
		
		# Start with an offset of 100 since we obtained the first batch already
		for ($i = 100; $i -lt $results.rangeInfo.Total; $i = $i + 100) {
			Write-Verbose "Calling for offset $i"
			$URI = BuildNewURI -Segments $URISegments -Parameters (BuildLimitOffsetParameters -Offset $i)
			$tResult = InvokeASARequest -URI $URI
			[void]$items.AddRange($tResult.items)
		}
		
		$results.items = $items
		$results.rangeInfo.limit = $results.rangeInfo.total
	}
	
	return $results
}


function ConvertToBase64 {
	[CmdletBinding()]
	[OutputType([string])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   Position = 0)]
		[string[]]$String
	)
	
	BEGIN {
	}
	
	PROCESS {
		foreach ($s in $String) {
			$bytes = [System.Text.Encoding]::UTF8.GetBytes($s)
			[System.Convert]::ToBase64String($bytes)
		}
	}
	
	END {
	}
}

function ConvertFromBase64 {
	[CmdletBinding()]
	[OutputType([string])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   Position = 0)]
		[string]$String
	)
	
	$bytes = [System.Convert]::FromBase64String($String)
	return [System.Text.Encoding]::UTF8.GetString($bytes)
}

#region IP Helper functions

function isValidIPv4Address {
	[CmdletBinding()]
	[OutputType([boolean])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   Position = 0)]
		[string[]]$IPAddress
	)
	
	Begin {
		
	}
	
	Process {
		foreach ($ip in $IPAddress) {
			if ((IPToBinary -IP $ip).Length -eq 32) {
				$true
			} else {
				Write-Verbose "Binary length not 32 bits"
				$false
			}
		}
	}
	
	End {
		
	}
}

function IPToBinary {
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$IP
	)
	
	$IP = $IP -replace '\s+' # remove whitespace for fun/flexibility
	try {
		# Check there are values in each of the 4 octets
		$IP.Split('.') | ForEach-Object {
			$i++
			if ($_.Length -eq 0) {
				throw "Invalid length for octet $i"
			}
		}
		
		return ($IP.Split('.') | ForEach-Object {
				[System.Convert]::ToString([byte]$_, 2).PadLeft(8, '0')
			}) -join ''
	} catch {
		Write-Warning -Message "Error converting '$IP' to a binary string: $_"
		return $null
	}
}

function isValidIPv4SubnetMask {
	[CmdletBinding(DefaultParameterSetName = 'Dotted')]
	param
	(
		[Parameter(ParameterSetName = 'Binary')]
		[string]$BinaryMask,
		
		[Parameter(ParameterSetName = 'Dotted')]
		[string]$DottedMask
	)
	
	switch ($PSCmdlet.ParameterSetName) {
		"Binary" {
			if ($BinaryMask -match '01') {
				Write-Verbose -Message "Invalid binary IPv4 subnet mask: '$BinaryMask'. Matched pattern '01'."
				$false
			} elseif ($BinaryMask.Length -ne 32) {
				Write-Verbose -Message "Invalid binary IPv4 subnet mask: '$BinaryMask'. Length was different from 32."
				$false
			} elseif ($BinaryMask -match '[^01]') {
				Write-Verbose -Message "Invalid binary IPv4 subnet mask: '$BinaryMask'. Was not all ones and zeroes."
				$false
			} else {
				$true
			}
		}
		
		"Dotted" {
			$Binary = IPToBinary -IP $DottedMask
			if ($Binary) {
				Test-IPv4SubnetMask -BinaryMask $Binary
			} else {
				$false
			}
		}
	}
}

#endregion

function ValidateNetworkObjectHostEntryValue {
	[CmdletBinding()]
	[OutputType([boolean])]
	param
	(
		[Parameter(Mandatory = $true)]
		[ValidateSet('IPv4Address', 'IPv4Range', 'IPv4Network', 'IPv4FQDN', 'IPv6Address', 'IPv6Range', 'IPv6Network')]
		[string]$Kind,
		
		[Parameter(Mandatory = $true)]
		[string]$Value
	)
	
	switch ($Kind) {
		'IPv4Address' {
			if (-not (isValidIPv4Address -IPAddress $Value)) {
				Write-Verbose "$Value is not valid"
				return $false
			}
			
			return $true
		}
		
		'IPV4Range' {
			$IPs = @($Value -split '-')
			foreach ($ip in $IPs) {
				if (-not (isValidIPv4Address -IPAddress $ip)) {
					Write-Verbose "$Value is not valid"
					return $false
				}
			}
			
			return $true
		}
		
		'IPv4Network' {
			$NetworkAddress = ($Value -split '/')[0]
			
			if (-not (isValidIPv4Address -IPAddress $NetworkAddress)) {
				Write-Verbose "NetworkAddress $NetworkAddress is not valid"
				return $false
			}
			
			if ($mask = ($Value -split '/')[1] -match '\.') {
				# This is a subnet mask... we will validate this later
				Write-Verbose 'Matched subnet mask... validating later...'
				if (isValidIPv4SubnetMask -DottedMask $mask) {
					return $true
				} else {
					Write-Verbose "Invalid subnet mask specified"
				}
				#} elseif (($Value -split '/')[1] -match '\A(?:3[0-2] | [12]?[0-9])\z') {
			} elseif ($CIDR = ($Value -split '/')[1] -in 0 .. 32) {
				# This is a valid CIDR entry
				Write-Verbose "Valid CIDR detected ($CIDR)"
				return $true
			} else {
				Write-Verbose "Need a CIDR value (0-32) or netmask value (x.x.x.x)"
				return $false
			}
		}
		
		'IPv4FQDN' {
			$DNSHostNameRegex = '^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$'
			
			if ($Value.Length -gt 255) {
				Write-Verbose "FQDN greater than 255 characters"
				return $false
			} elseif ($Value -notmatch $DNSHostNameRegex) {
				Write-Verbose "Failed DNS regex"
				return $false
			}
			
			return $true
		}
		
		'IPv6Address' {
			return $true
		}
		
		'IPv6Range' {
			return $true
		}
		
		'IPv6Network' {
			return $true
		}
		
		default {
			throw "Cannot validate unknown kind $Kind"
		}
	}
}

function ValidateNetworkObjectHostEntryObject {
	[CmdletBinding()]
	[OutputType([bool])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[pscustomobject]$HostObject
	)
	
	# Ensure the host object in the network object is valid.
	
	if (($HostObject.psobject.properties.name).count -ne 2) {
		throw "Expected two properties for host object"
	}
	
	if ($HostObject.psobject.properties.name -notcontains 'value') {
		throw "Missing value property for host object"
	}
	
	if ($HostObject.psobject.properties.name -notcontains 'kind') {
		throw "Missing kind property for host object"
	}
	
	# Okay, so we have only two properties with names 'value' and 'kind'
	# Ensure these are correct...
	if (-not (ValidateNetworkObjectHostEntryValue -Kind $HostObject.Kind -Value $HostObject.Value)) {
		return $false
	}
	
	return $true
}

