﻿

function Get-ASANetworkObject {
<#
	.SYNOPSIS
		Fetch a network object
	
	.DESCRIPTION
		Obtains one or all network objects. Without specifying 'ObjectId' or 'All' parameters,
		this will return the default limit of 100 objects. The 'All' switch will send multiple
		API calls to fetch objects if the total count is greater than 100.
	
	.PARAMETER ObjectId
		The object ID of the network object to be fetched
	
	.PARAMETER All
		Return all network objects
	
	.PARAMETER Limit
		Number of items to return; maximum of 100
	
	.PARAMETER Offset
		Index of first item to return
	
	.EXAMPLE
		PS C:\> Get-ASANetworkObject -ObjectId 'Value1'
	
	.OUTPUTS
		pscustomobject, pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'Default')]
	[OutputType([pscustomobject], ParameterSetName = 'All')]
	[OutputType([pscustomobject], ParameterSetName = 'Single')]
	[OutputType([pscustomobject], ParameterSetName = 'Default')]
	param
	(
		[Parameter(ParameterSetName = 'Single', Mandatory = $true)]
		[string]$ObjectId,
		
		[Parameter(ParameterSetName = 'All')]
		[switch]$All,
		
		[Parameter(ParameterSetName = 'Default')]
		[ValidateRange(0, 100)]
		[uint16]$Limit,
		
		[Parameter(ParameterSetName = 'Default')]
		[uint16]$Offset
	)
	
	$URISegments = [System.Collections.ArrayList]::new(@('objects', 'networkobjects'))
	
	switch ($PsCmdlet.ParameterSetName) {
		'Single' {
			$URI = BuildNewURI -Segments ($URISegments + $ObjectId)
			break
		}
		
		'All' {
			# This will run a loop
			break
		}
		
		'Default' {
			$URI = BuildNewURI -Segments $URISegments -Parameters (BuildLimitOffsetParameters $Limit $Offset)
			break
		}
	}
	
	if ($PSCmdlet.ParameterSetName -ne 'All') {
		return InvokeASARequest -URI $URI
	}
	
	return LoopASARequestUntilAllItemsObtained -URISegments $URISegments
}


function New-ASANetworkObject {
<#
	.SYNOPSIS
		Create a new network object
	
	.DESCRIPTION
		Creates a network object which can contain a single HostEntry of any one kind
	
	.PARAMETER Name
		The name of the object.
	
	.PARAMETER HostEntry
		The network object HostEntry.
	
	.PARAMETER Description
		A description for the network object if desired
	
	.PARAMETER Kind
		The kind of network object. Currently only 'obj#NetworkObj' supported
	
	.PARAMETER PassThru
		Returns a PSCustomObject for the created object. By default, nothing is returned.
	
	.EXAMPLE
		PS C:\> New-ASANetworkObject -Kind 'Value1'
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(SupportsShouldProcess = $true)]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$Name,
		
		[Parameter(Mandatory = $true)]
		[pscustomobject]$HostEntry,
		
		[string]$Description,
		
		[Parameter(Mandatory = $false)]
		[ValidateSet('object#NetworkObj')]
		[string]$Kind = 'object#NetworkObj',
		
		[switch]$PassThru
	)
	
	if (-not (ValidateNetworkObjectHostEntryObject -HostObject $HostEntry)) {
		throw "Invalid host object supplied"
	}
	
	$object = [pscustomobject]@{
		'name' = $Name
		'kind' = $Kind
		'host' = $HostEntry
	}
	
	if ($Description) {
		$object.description = $Description
	}
	
	if ($PSCmdlet.ShouldProcess($Name, 'New network object')) {
		$result = InvokeASARequest -URI (BuildNewURI -Segments 'objects', 'networkobjects') -Body $object -Method POST -Verbose:$VerbosePreference
		if ($PassThru) {
			# We will need to get the network object from the ASA because it returns nothing by default
			Get-ASANetworkObject -ObjectId $Name -Verbose:$VerbosePreference
		} else {
			$result
		}
	}
}

function New-ASANetworkObjectHost {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[ValidateSet('IPv4Address', 'IPv4Range', 'IPv4Network', 'IPv4FQDN', 'IPv6Address', 'IPv6Range', 'IPv6Network')]
		[string]$Kind,
		
		[Parameter(Mandatory = $true)]
		[string]$Value
	)
	
	
	if (-not (ValidateNetworkObjectHostEntryValue -Kind $Kind -Value $Value)) {
		throw "Invalid data ($Value) for kind $Kind"
	}
	
	[pscustomobject]@{
		'kind' = $Kind
		'value' = $Value
	}
}

function Remove-ASANetworkObject {
	[CmdletBinding(DefaultParameterSetName = 'FromObject',
				   ConfirmImpact = 'High',
				   SupportsShouldProcess = $true)]
	param
	(
		[Parameter(ParameterSetName = 'FromObject',
				   Mandatory = $true,
				   ValueFromPipeline = $true)]
		[pscustomobject]$NetworkObject,
		
		[Parameter(ParameterSetName = 'FromStrings',
				   Mandatory = $true)]
		[string]$ObjectId
	)
	
	if ($pscmdlet.ShouldProcess("Target", "Operation")) {
		#TODO: Place script here
	}
	
	switch ($PSCmdlet.ParameterSetName) {
		"FromObject" {
			# Only validate the object
			
		}
		
		"FromString" {
			# Attempt to get the object from the ASA
		}
	}
	
	
}



