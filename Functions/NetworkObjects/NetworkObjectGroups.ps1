﻿

function Get-ASANetworkObjectGroup {
<#
	.SYNOPSIS
		Fetch a network object group
	
	.DESCRIPTION
		Obtains one or all network object groups
	
	.PARAMETER ObjectId
		The object ID of the network object group to be fetched
	
	.PARAMETER All
		Return all network objects
	
	.PARAMETER Limit
		Number of items to return; maximum of 100
	
	.PARAMETER Offset
		Index of first item to return
	
	.EXAMPLE
		PS C:\> Get-ASANetworkObjectGroup -ObjectId 'Value1'
	
	.OUTPUTS
		pscustomobject, pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'Default')]
	[OutputType([pscustomobject], ParameterSetName = 'All')]
	[OutputType([pscustomobject], ParameterSetName = 'Single')]
	[OutputType([pscustomobject], ParameterSetName = 'Default')]
	param
	(
		[Parameter(ParameterSetName = 'Single', Mandatory = $true)]
		[string]$ObjectId,
		
		[Parameter(ParameterSetName = 'All')]
		[switch]$All,
		
		[Parameter(ParameterSetName = 'Default')]
		[ValidateRange(1, 100)]
		[uint16]$Limit,
		
		[Parameter(ParameterSetName = 'Default')]
		[uint16]$Offset
	)
	
	$URISegments = [System.Collections.ArrayList]::new(@('objects', 'networkobjectgroups'))
	
	switch ($PsCmdlet.ParameterSetName) {
		'Single' {
			$URI = BuildNewURI -Segments ($URISegments + $ObjectId)
			break
		}
		
		'All' {
			# This will run a loop
			break
		}
		
		'Default' {
			$URI = BuildNewURI -Segments $URISegments -Parameters (BuildLimitOffsetParameters $Limit $Offset)
			break
		}
	}
	
	if ($PSCmdlet.ParameterSetName -ne 'All') {
		return InvokeASARequest -URI $URI
	}
	
	return LoopASARequestUntilAllItemsObtained -URISegments $URISegments
}







