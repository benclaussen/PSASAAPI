﻿

function Get-ASAExtendedACL {
<#
	.SYNOPSIS
		Fetch an extended ACL
	
	.DESCRIPTION
		Fetch one or all extended ACLs
	
	.PARAMETER ObjectId
		The object ID of the extended ACL message to be fetched
	
	.PARAMETER All
		Return all Extended ACLs
	
	.PARAMETER Limit
		Number of items to return; maximum of 100
	
	.PARAMETER Offset
		Index of first item to return
	
	.EXAMPLE
		PS C:\> Get-ASAExtendedACL -ObjectId 'Value1'
	
	.OUTPUTS
		pscustomobject, pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'All')]
	[OutputType([pscustomobject], ParameterSetName = 'Single')]
	[OutputType([pscustomobject], ParameterSetName = 'All')]
	param
	(
		[Parameter(ParameterSetName = 'Single',
				   Mandatory = $true)]
		[string]$ObjectId,
		
		[Parameter(ParameterSetName = 'All')]
		[ValidateRange(1, 100)]
		[uint16]$Limit,
		
		[Parameter(ParameterSetName = 'All')]
		[uint16]$Offset
	)
	
	switch ($PsCmdlet.ParameterSetName) {
		'Single' {
			$URI = BuildNewURI -Segments 'objects', 'extendedacls', $ObjectId
			break
		}
		
		'All' {
			$URI = BuildNewURI -Segments 'objects', 'extendedacls' -Parameters (BuildLimitOffsetParameters $Limit $Offset)
			break
		}
	}
	
	InvokeASARequest -URI $URI
}

function Get-ASAExtendedACLEntries {
<#
	.SYNOPSIS
		Fetch an ACE on an interface
	
	.DESCRIPTION
		Fetch one or all ACEs in an ACL
	
	.PARAMETER ACLName
		The name of the ACL to which this rule belongs
	
	.PARAMETER Single
		A description of the Single parameter.
	
	.PARAMETER ObjectId
		A description of the ObjectId parameter.
	
	.PARAMETER All
		A description of the All parameter.
	
	.PARAMETER Limit
		Number of items to return; maximum of 100
	
	.PARAMETER Offset
		Index of first item to return
	
	.EXAMPLE
		PS C:\> Get-ASAExtendedACLEntries
	
	.OUTPUTS
		pscustomobject, pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'Default')]
	[OutputType([pscustomobject], ParameterSetName = 'All')]
	[OutputType([pscustomobject], ParameterSetName = 'Single')]
	[OutputType([pscustomobject], ParameterSetName = 'Default')]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$ACLName,
		
		[Parameter(ParameterSetName = 'Single', Mandatory = $true)]
		[string]$ObjectId,
		
		[Parameter(ParameterSetName = 'All')]
		[switch]$All,
		
		[Parameter(ParameterSetName = 'Default')]
		[ValidateRange(0, 100)]
		[uint16]$Limit,
		
		[Parameter(ParameterSetName = 'Default')]
		[uint16]$Offset
	)
	
	$URISegments = [System.Collections.ArrayList]::new(@('objects', 'extendedacls'))
	
	switch ($PsCmdlet.ParameterSetName) {
		'Single' {
			$URI = BuildNewURI -Segments ($URISegments + $ACLName + 'aces' + $ObjectId)
			break
		}
		
		'All' {
			# This will run a loop
			break
		}
		
		'Default' {
			$URI = BuildNewURI -Segments ($URISegments + $ACLName + 'aces') -Parameters (BuildLimitOffsetParameters $Limit $Offset)
			break
		}
	}
	
	if ($PSCmdlet.ParameterSetName -ne 'All') {
		return InvokeASARequest -URI $URI
	}
	
	return LoopASARequestUntilAllItemsObtained -URISegments ($URISegments + $ACLName + 'aces')
}



