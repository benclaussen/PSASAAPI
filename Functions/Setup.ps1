﻿

function SetupASAConfigVariable {
	[CmdletBinding()]
	param ()
	
	Write-Verbose "Checking for ASAConfig hashtable"
	if (-not ($script:ASAConfig)) {
		Write-Verbose "Creating ASAConfig hashtable"
		$script:ASAConfig = @{
			'Connected' = $false
		}
	}
	
	Write-Verbose "ASAConfig hashtable already exists"
}

function Set-ASAHostName {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$Hostname
	)
	
	$script:ASAConfig.Hostname = $Hostname.Trim()
	$script:ASAConfig.Hostname
}

function Get-ASAHostname {
	[CmdletBinding()]
	param ()
	
	if ($script:ASAConfig.Hostname -eq $null) {
		throw "ASA Hostname is not set! You may set it with Set-ASAHostname -Hostname 'hostname.domain.tld'"
	}
	
	$script:ASAConfig.Hostname
}

function Set-ASACredentials {
	[CmdletBinding(DefaultParameterSetName = 'CredsObject')]
	[OutputType([pscredential], ParameterSetName = 'CredsObject')]
	[OutputType([pscredential], ParameterSetName = 'UserPass')]
	param
	(
		[Parameter(ParameterSetName = 'CredsObject',
				   Mandatory = $true)]
		[pscredential]$Credentials,
		
		[Parameter(ParameterSetName = 'UserPass',
				   Mandatory = $true)]
		[string]$Username,
		
		[Parameter(ParameterSetName = 'UserPass',
				   Mandatory = $true)]
		[string]$Password
	)
	
	switch ($PsCmdlet.ParameterSetName) {
		'CredsObject' {
			$script:ASAConfig.Credentials = $Credentials
			break
		}
		'UserPass' {
			$securePW = ConvertTo-SecureString $Password -AsPlainText -Force
			$script:ASAConfig.Credentials = [System.Management.Automation.PSCredential]::new($Username, $securePW)
			break
		}
	}
	
	$script:ASAConfig.Credentials
}

function Get-ASACredentials {
	[CmdletBinding()]
	param ()
	
	if (-not $script:ASAConfig.Credentials) {
		throw "ASA Credentials not set! You may set with Set-ASACredentials"
	}
	
	$script:ASAConfig.Credentials
}

function Set-ASAToken {
	[CmdletBinding()]
	[OutputType([string])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$Token
	)
	
	$script:ASAConfig.Token = $Token
	
	return $script:ASAConfig.Token
}

function Get-ASAToken {
	[CmdletBinding()]
	param ()
	
	if (-not $script:ASAConfig.Token) {
		throw "ASA Token not set! You may set with Set-ASAToken"
	}
	
	$script:ASAConfig.Token
}

function GetTokenFromASA {
	[CmdletBinding()]
	[OutputType([string])]
	param ()
	
	$URI = BuildNewURI -Segments 'tokenservices'
	
	$response = InvokeASARequest -URI $URI -Method POST -TokenRequest
	
	return $response.Headers.'X-Auth-Token'
}


function Connect-ASAAPI {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$Hostname,
		
		[Parameter(Mandatory = $false)]
		[pscredential]$Credentials
	)
	
	SetupASAConfigVariable
	
	if (-not $Credentials) {
		if (-not ($Credentials = Get-Credential -Message "Enter credentials for ASA")) {
			throw "Credentials are necessary to connect to an ASA API service."
		}
	}
	
	$null = Set-ASAHostName -Hostname $Hostname
	
	$null = Set-ASACredentials -Credentials $Credentials
	
	try {
		$null = Set-ASAToken -Token (GetTokenFromASA -ErrorAction Stop)
		return $true
	} catch {
		if (($_.Exception.Response) -and ($_.Exception.Response.StatusCode -eq 401)) {
			throw "Invalid username or password"
		} else {
			throw $_
		}
	}
}




