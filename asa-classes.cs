using System;

namespace PSASA
{
    public enum ObjectKind
    {
        IPv4Address,
        IPv4Range,
        IPv4Network,
        IPv4FQDN,
        IPv6Address,
        IPv6Range,
        IPv6Network
    }

    namespace NetworkObject
    {
        public enum Kind
        {
            object_NetworkObj,
            objectRef_NetworkObj,
            objectRef_Interface
        }

        namespace Host
        {
            public enum Kind
            {
                IPv4Address = 0,
                IPv4Range = 1,
                IPv4Network = 2,
                IPv4FQDN = 3,
                IPv6Address = 4,
                IPv6Range = 5,
                IPv6Network = 6
            }
        }
    }

    namespace ACE
    {
        public enum Kind
        {
            object_ExtendedACE
        }

        namespace Address
        {
            public enum Kind
            {
                AnyIPAddress,
                IPv4Address,
                IPv4Network,
                objectRef_NetworkObj,
                objectRef_NetworkObjGroup
            }
        }

        namespace Service
        {
            public enum Kind
            {
                NetworkProtocol,
                TcpUdpService,
                objectRef_NetworkServiceGroup
            }
        }

        public class ExtendedACE
        {
        }
    }
}